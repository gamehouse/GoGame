package main

import (
	"fmt"
	_ "image/png"
	"log"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

const (
	screenWidth  = 480
	screenHeight = 300
)

var (
	playerImage, _, err    = ebitenutil.NewImageFromFile("assets/PlayerDown.png", ebiten.FilterDefault) // This is the player's default sprite
	playerImageDrawOptions = &ebiten.DrawImageOptions{}                                                 // This thing stores stuff like coordinates and all
	playerDefense          = 10       // How much damage the player can absorb
	playerAttack           = 20       // How much damage the player deals
	playerHealth           = 250      // How much health the player has
	playerInventoryDisplayArray [5] string
	bkgImage, bkgOther1, bkgOther2 = ebitenutil.NewImageFromFile("assets/map1.png", ebiten.FilterDefault)
	bkgImageDrawOptions    = &ebiten.DrawImageOptions{}
)

func update(screen *ebiten.Image) error {
	// Handle input
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		playerImage, _, err = ebitenutil.NewImageFromFile("assets/PlayerLeft.png", ebiten.FilterDefault)
		bkgImageDrawOptions.GeoM.Translate(3, 0)
	}
	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		playerImage, _, err = ebitenutil.NewImageFromFile("assets/PlayerRight.png", ebiten.FilterDefault)
		bkgImageDrawOptions.GeoM.Translate(-3, 0)
	}
	if ebiten.IsKeyPressed(ebiten.KeyUp) {
		playerImage, _, err = ebitenutil.NewImageFromFile("assets/PlayerUp.png", ebiten.FilterDefault)
		bkgImageDrawOptions.GeoM.Translate(0, 3)
	}
	if ebiten.IsKeyPressed(ebiten.KeyDown) {
		playerImage, _, err = ebitenutil.NewImageFromFile("assets/PlayerDown.png", ebiten.FilterDefault)
		bkgImageDrawOptions.GeoM.Translate(0, -3)
	}

	// Determine if rendering is necessary
	if ebiten.IsDrawingSkipped() {
		return nil
	}
	
	// Draw the background
	screen.DrawImage(bkgImage, bkgImageDrawOptions)

	if ebiten.IsKeyPressed(ebiten.KeyC) {
		msg := fmt.Sprintf("\n\nDEF: %v", playerDefense)
		ebitenutil.DebugPrint(screen, msg)
		
		msg = fmt.Sprintf("\n\n\nATK: %v", playerAttack)
		ebitenutil.DebugPrint(screen, msg)
	}
	if ebiten.IsKeyPressed(ebiten.KeyE) {
		msg := fmt.Sprintf("\n\n1st Item: %v", playerInventoryDisplayArray[0])
		ebitenutil.DebugPrint(screen, msg)
		
		msg = fmt.Sprintf("\n\n\n2nd Item: %v", playerInventoryDisplayArray[1])
		ebitenutil.DebugPrint(screen, msg)
		
		msg = fmt.Sprintf("\n\n\n\n3rd Item: %v", playerInventoryDisplayArray[2])
		ebitenutil.DebugPrint(screen, msg)
		
		msg = fmt.Sprintf("\n\n\n\n\n4th Item: %v", playerInventoryDisplayArray[3])
		ebitenutil.DebugPrint(screen, msg)
			
		msg = fmt.Sprintf("\n\n\n\n\n\n5th Item: %v", playerInventoryDisplayArray[4])
		ebitenutil.DebugPrint(screen, msg)
	}
	
	// Draw the player
	screen.DrawImage(playerImage, playerImageDrawOptions)

	// Writing the FPS
	msg := fmt.Sprintf("FPS: %0.2f", ebiten.CurrentFPS())
	ebitenutil.DebugPrint(screen, msg)
	
	// Writing the player's health
	msg = fmt.Sprintf("\nHP: %v", playerHealth)
	ebitenutil.DebugPrint(screen, msg)
	return nil
}

func main() {
	playerInventoryDisplayArray[0] = "Basic Assault Rifle"
	playerInventoryDisplayArray[1] = "Basic Armour"
	playerInventoryDisplayArray[2] = "Basic Holo Shield (rifle accessory)"
	playerInventoryDisplayArray[3] = "Ánti Fog Visor"
	playerInventoryDisplayArray[4] = "Replacement Plasma Cartridge (rifle ammo)"
	playerImageDrawOptions.GeoM.Translate(screenWidth / 2 - 10, screenHeight / 2 - 15)
	if err := ebiten.Run(update, screenWidth, screenHeight, 2, "Game (not sure what to call it yet)"); err != nil {
		log.Fatal(err)
	}
}
